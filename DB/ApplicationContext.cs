﻿using DZ6.DB;
using DZ6.Entitys;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;


namespace DZ6
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Currency> Currencys { get; set; }
        public DbSet<TypeClient> TypeClients { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Client> Clients { get; set; }
        public ApplicationContext()
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            string connectionString = config.GetConnectionString("BaseConnection");

            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(ent =>
            {
                ent.ToTable("Clients").HasComment("Для информации по клиентам");
                ent.HasKey(c => c.Id).HasName("ClientPrimaryKey");
                ent.HasAlternateKey(c => new { c.FirstName, c.Email });
                ent.HasIndex(c => c.Passport).IsUnique();
                ent.Property(c => c.Passport).HasMaxLength(50).IsRequired();
                ent.Property(c => c.FirstName).HasMaxLength(150).IsRequired();
                ent.Property(c => c.LastName).HasMaxLength(150).IsRequired();
                ent.Property(c => c.Patronymic).HasMaxLength(150);
                ent.Property(c => c.Email).HasMaxLength(30);
                ent.Property(c => c.ResidFl);
                ent.Property(c => c.Address).HasMaxLength(500);
                ent.Property(c => c.TypeClientId).HasColumnName("Typeclient_id");
                ent.HasOne(c => c.TypeClient);
            });

            modelBuilder.Entity<Account>(ent =>
            {
                ent.ToTable("Accounts").HasComment("Для информации по счетам");
                ent.HasKey(a => a.Id);
                ent.Property(a => a.Code).HasMaxLength(30).IsRequired();
                ent.Property(a => a.Description).HasMaxLength(250);
                ent.Property(a => a.StateFl).HasDefaultValue("True");
                ent.HasOne(c => c.Client).WithMany(a => a.Accounts).HasForeignKey(k => k.ClientId).HasConstraintName("accounts_fk_client_id_fkey");
                ent.HasOne(a => a.Currency).WithOne().HasConstraintName("accounts_fk_currency_id_key");
            });

            modelBuilder.Entity<TypeClient>(ent =>
            {
                ent.HasComment("Тип клиента (Юр.лицо/Физ.лицо/ИП)");
                ent.HasKey(t => t.Id);
                ent.Property(t => t.TypeCode).HasMaxLength(30).IsRequired();
                ent.Property(t => t.TypeName).HasMaxLength(150);
            });

            modelBuilder.Entity<Currency>(ent =>
            {
                ent.ToTable("Currencys").HasComment("Валюта");
                ent.HasKey(cr => cr.Id);
                ent.HasIndex(cr => cr.Code).IsUnique();
                ent.Property(cr => cr.Code).HasMaxLength(3).IsRequired();
                ent.Property(cr => cr.LongName).HasMaxLength(150);
            });

            base.OnModelCreating(modelBuilder);
        }
        public void ContextDefaultCurrency()
        {
            //Добавление данных
            Currency kzt = new Currency { Code = "KZT", LongName = "Казахстанский тенге" };
            Currency rub = new Currency { Code = "RUB", LongName = "Российский рубль" };
            Currency usd = new Currency { Code = "USD", LongName = "Доллар  США" };
            Currency eur = new Currency { Code = "EUR", LongName = "ЕВРО" };
            Currency kgs = new Currency { Code = "KGS", LongName = "Кыргызский сом" };
            Currency byn = new Currency { Code = "BYN", LongName = "Белорусский рубль" };
            //добавляем в БД
            this.Currencys.AddRange(kzt, rub, usd, eur, kgs, byn);
            this.SaveChanges();
        }
        public void ContextDefaultTypeCli()
        {
            //Добавление данных
            TypeClient fiz = new TypeClient { TypeCode = "FIZ", TypeName = "Физ. лицо" };
            TypeClient jur = new TypeClient { TypeCode = "JUR", TypeName = "Юр. лицо" };
            TypeClient ip = new TypeClient { TypeCode = "IP", TypeName = "ИП" };
            //добавляем в БД
            this.TypeClients.AddRange(fiz, jur, ip);
            this.SaveChanges();
        }

        public void ContextDefaultClient()
        {
            //Добавление данных
            GenericTable<TypeClient> tp = new GenericTable<TypeClient>(this);
            var typecli = tp.GetList(x => x.TypeCode == "FIZ")[0];
            Client client1 = new Client
            {
                ResidFl = true,
                FirstName = "Власов",
                LastName = "Денис",
                Patronymic = "Петрович",
                DateOfBirth = new DateTime(1985, 6, 24),
                Address = "Республика Казахстан, город Алматы, проспект Достык",
                Email = "den_vp@mail.ru",
                Passport = "N123456789",
                TypeClientId = typecli.Id
            };

            Client client2 = new Client
            {
                ResidFl = false,
                FirstName = "Иванов",
                LastName = "Иван",
                Patronymic = "Иванович",
                DateOfBirth = new DateTime(1990, 3, 5),
                Address = "Россия",
                Email = "test@mail.ru",
                Passport = "N987654321",
                TypeClientId = typecli.Id
            };

            //добавляем в БД
            this.Clients.AddRange(client1, client2);
            this.SaveChanges();
        }

        public void ContextDefaultAccount()
        {
            //Добавление данных
            GenericTable<Client> tp = new GenericTable<Client>(this);
            var cli = tp.GetList(x => x.Passport == "N123456789")[0];
            GenericTable<Currency> cur = new GenericTable<Currency>(this);
            var val = cur.GetList(x => x.Code == "KZT")[0];
            Account acc1 = new Account
            {   ClientId = cli.Id,
                CurrencyId = val.Id,
                DateOpen = DateTime.Now,
                Code = $"KZ12345{val.Code}{cli.Id}123456789",
                Description = "Тестовый счет 1"
            };

            val = cur.GetList(x => x.Code == "USD")[0];
            Account acc2 = new Account
            {
                ClientId = cli.Id,
                CurrencyId = val.Id,
                DateOpen = DateTime.Now,
                Code = $"KZ78945{val.Code}{cli.Id}123456789",
                Description = "Тестовый счет 2"
            };

            cli = tp.GetList(x => x.Passport == "N987654321")[0];
            val = cur.GetList(x => x.Code == "RUB")[0];
            Account acc3 = new Account
            {
                ClientId = cli.Id,
                CurrencyId = val.Id,
                DateOpen = DateTime.Now,
                Code = $"RU12345{val.Code}{cli.Id}123456789",
                Description = "Тестовый счет 3"
            };
            val = cur.GetList(x => x.Code == "EUR")[0];
            Account acc4 = new Account
            {
                ClientId = cli.Id,
                CurrencyId = val.Id,
                DateOpen = DateTime.Now,
                Code = $"RU12345{val.Code}{cli.Id}123456789",
                Description = "Тестовый счет 4"
            };

            //добавляем в БД
            this.Accounts.AddRange(acc1, acc2, acc3, acc4);
            this.SaveChanges();
        }

    }
}