﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6.DB
{
    public class GenericTable<TEntity> : IGenericTable<TEntity> where TEntity : class
    {
        DbContext _context;
        DbSet<TEntity> _dbSet;

        public GenericTable(ApplicationContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public void Create(TEntity ent)
        {
            _dbSet.Add(ent);
            _context.SaveChanges();
        }
        public void Remove(TEntity ent)
        {
            _dbSet.Remove(ent);
            _context.SaveChanges();
        }

        public void Update(TEntity ent)
        {
            _context.Entry(ent).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public List<TEntity> GetList()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        public List<TEntity> GetList(Func<TEntity, bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }
    }
}
