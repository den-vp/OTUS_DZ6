﻿using DZ6.DB;
using DZ6.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DZ6
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                GenericTable<Currency> cur = new GenericTable<Currency>(db);
                var currency = cur.GetList();
                if (currency.Count == 0)
                {
                    db.ContextDefaultCurrency();
                    currency = cur.GetList();
                }
                Console.WriteLine("Возможные валюты:");
                foreach (Currency item in currency)
                {
                    Console.WriteLine($"ID {item.Id}; Код валюты - {item.Code}; Описание - {item.LongName};");
                }

                GenericTable<TypeClient> tp = new GenericTable<TypeClient>(db);
                var typecli = tp.GetList();
                if (typecli.Count == 0)
                {
                    db.ContextDefaultTypeCli();
                    typecli = tp.GetList();
                }
                Console.WriteLine("Возможные типы клиента:");
                foreach (TypeClient item in typecli)
                {
                    Console.WriteLine($"ID {item.Id}; Код - {item.TypeCode}; Описание - {item.TypeName};");
                }


                GenericTable<Client> cl = new GenericTable<Client>(db);
                var cli = cl.GetList();
                if (cli.Count == 0)
                {
                    db.ContextDefaultClient();
                    cli = cl.GetList();
                }
                Console.WriteLine("Клиенты:");
                foreach (Client item in cli)
                {
                    Console.WriteLine($"ID {item.Id}; Имя - {item.LastName}; Фамилия - {item.FirstName}; Паспорт - {item.Passport};");
                }

                GenericTable<Account> account = new GenericTable<Account>(db);
                var acc = account.GetList();
                if (acc.Count == 0)
                {
                    db.ContextDefaultAccount();
                    acc = account.GetList();
                }
                Console.WriteLine("Счета:");
                foreach (Account item in acc)
                {
                    var client = cl.GetList(x => x.Id == item.ClientId)[0];
                    Console.WriteLine($"ID {item.Id}; Номер счета {item.Code}; Клиент - {client.FirstName} {client.LastName};");
                }
                while (true)
                {
                    Console.WriteLine("Для добавления нового клиента введите 1.");
                    Console.WriteLine("Для добавления нового счета клиенту введите 2.");
                    Console.WriteLine("Для изменения клиента 3.");
                    Console.WriteLine("Для удаления счета введите 4.");
                    Console.WriteLine("Для отображения клиента/клиентов введите 5.");
                    Console.WriteLine("Введите 6 для выхода.");
                    var s = Console.ReadLine();
                    if (string.IsNullOrEmpty(s) || !int.TryParse(s, out var choice) || choice < 0 || choice > 6)
                    {
                        Console.WriteLine("Необходимо ввести число от 1 до 6.");
                        continue;
                    }

                    switch (choice)
                    {
                        case 1:
                            Console.Write("Введите имя: ");
                            string firstName = Console.ReadLine();
                            Console.Write("Введите фамилию: ");
                            string lastName = Console.ReadLine();
                            Console.Write("Введите отчество: ");
                            string patronymic = Console.ReadLine();
                            DateTime _datebirth;
                            while (true)
                            {
                                Console.Write("Введите Дату рождения: ");
                                string datebirth = Console.ReadLine();
                                if (DateTime.TryParse(datebirth, out _datebirth) && !string.IsNullOrEmpty(datebirth)) break;
                                else
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                            }
                            while (true)
                            {
                                Console.Write("Введите резиденство (true - резидент/false - не резидент): ");
                                string resfl = Console.ReadLine();
                                if (resfl.Equals("true") || resfl.Equals("false")) break;
                                else
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                            }
                            Console.Write("Введите адрес : ");
                            string address = Console.ReadLine();
                            Console.Write("Введите email: ");
                            string email = Console.ReadLine();
                            Console.Write("Введите номер паспорта: ");
                            string passport = Console.ReadLine();

                            while (true)
                            {
                                Console.WriteLine("Введите TypeCode типа клиента: ");
                                foreach (TypeClient item in tp.GetList())
                                {
                                    Console.WriteLine($"ID {item.Id}; TypeCode - {item.TypeCode}; Описание - {item.TypeName};");
                                }
                                string typecl = Console.ReadLine();
                                typecli = tp.GetList(x => x.TypeCode == typecl);
                                if (typecli.Count() == 0)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else break;
                            }
                            Client clientnew = new Client
                            {
                                FirstName = firstName,
                                LastName = lastName,
                                Patronymic = patronymic,
                                DateOfBirth = _datebirth,
                                Address = address,
                                Email = email,
                                Passport = passport,
                                TypeClientId = typecli[0].Id
                            };
                            cl.Create(clientnew);
                            Console.WriteLine("Клиент сохранен");
                            break;
                        case 2:
                            List<Client> clinew;
                            while (true)
                            {
                                Console.WriteLine("Введите ID клиента: ");
                                foreach (Client item in cl.GetList())
                                {
                                    Console.WriteLine($"ID {item.Id}; Фамилия - {item.FirstName}; Имя - {item.LastName};");
                                }
                                int idCli;
                                try
                                {
                                    idCli = Convert.ToInt32(Console.ReadLine());
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                clinew = cl.GetList(x => x.Id == idCli);
                                if (clinew.Count() == 0)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else break;
                            }
                            int idVal;
                            string codeVal;
                            while (true)
                            {
                                Console.WriteLine("Введите ID Валюты: ");
                                foreach (Currency item in currency)
                                {
                                    Console.WriteLine($"ID {item.Id}; Код валюты - {item.Code}; Описание - {item.LongName};");
                                }
                                try
                                {
                                    idVal = Convert.ToInt32(Console.ReadLine());
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                var curnew = cur.GetList(x => x.Id == idVal);
                                if (curnew.Count() == 0)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else
                                {
                                    codeVal = curnew[0].Code;
                                    break;
                                }
                            }
                            Console.WriteLine("Введите описание счета: ");
                            string description = Console.ReadLine();
                            Random rand = new Random();
                            string prefix = clinew[0].ResidFl == true ? "KZ" : "RU";
                            Account accountnew = new Account
                            {
                                ClientId = clinew[0].Id,
                                CurrencyId = idVal,
                                DateOpen = DateTime.Now,
                                Code = $"{prefix}{rand.Next(00001, 99999)}{codeVal}{clinew[0].Id}{rand.Next(000000001, 999999999)}",
                                
                                Description = description
                            };
                            account.Create(accountnew);
                            Console.WriteLine("Счет сохранен");

                            break;
                        case 3:
                            int idUpdCli;
                            List<Client> cliupd;
                            while (true)
                            {
                                Console.WriteLine("Введите ID клиента: ");
                                foreach (Client item in cl.GetList())
                                {
                                    Console.WriteLine($"ID {item.Id}; Фамилия - {item.FirstName}; Имя - {item.LastName};");
                                }
                                try
                                {
                                    idUpdCli = Convert.ToInt32(Console.ReadLine());
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                cliupd = cl.GetList(x => x.Id == idUpdCli);
                                if (cliupd.Count() == 0)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else break;
                            }

                            Console.Write($"Имя клиента {cliupd[0].FirstName}; Введите новое имя (или оставьте пустым если не нужно изменять): ");
                            string updfirstName = Console.ReadLine();
                            Console.Write($"Фамилия клиента {cliupd[0].LastName}; Введите новую фамилию (или оставьте пустым если не нужно изменять): ");
                            string updlastName = Console.ReadLine();
                            Console.Write($"Отчество клиента {cliupd[0].Patronymic}; Введите новое отчество (или оставьте пустым если не нужно изменять): ");
                            string updpatronymic = Console.ReadLine();

                            DateTime _upddatebirth;
                            string upddatebirth;
                            while (true)
                            {
                                Console.Write($"Дата рождения {cliupd[0].DateOfBirth}; Введите новую Дату рождения (или оставьте пустым если не нужно изменять): ");
                                upddatebirth = Console.ReadLine();
                                if (DateTime.TryParse(upddatebirth, out _upddatebirth) && !string.IsNullOrEmpty(upddatebirth)) break;
                                else
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                            }

                            while (true)
                            {
                                Console.Write($"Резиденство {cliupd[0].ResidFl}; Введите новое резиденство (true - резидент/talse - не резидент или оставьте пустым если не нужно изменять): ");
                                string resfl = Console.ReadLine();
                                if (resfl.Equals("true") || resfl.Equals("false") || string.IsNullOrEmpty(resfl)) break;
                                else
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                            }

                            Console.Write($"Адрес клиента {cliupd[0].Address}; Введите новый адрес (или оставьте пустым если не нужно изменять): ");
                            string updaddress = Console.ReadLine();
                            Console.Write($"Email клиента {cliupd[0].Email}; Введите новый email (или оставьте пустым если не нужно изменять): ");
                            string updemail = Console.ReadLine();
                            Console.Write($"Номер паспорта клиента {cliupd[0].Passport}; Введите новый номер паспорта (или оставьте пустым если не нужно изменять): ");
                            string updpassport = Console.ReadLine();
                            string typeclient;
                            while (true)
                            {
                                Console.WriteLine($"Типа клиента {tp.GetList(x => x.Id == cliupd[0].TypeClientId)[0].TypeCode}; Введите новый TypeCode типа клиента (или оставьте пустым если не нужно изменять): ");
                                foreach (TypeClient item in tp.GetList())
                                {
                                    Console.WriteLine($"ID {item.Id}; TypeCode - {item.TypeCode}; Описание - {item.TypeName};");
                                }
                                typeclient = Console.ReadLine();
                                typecli = tp.GetList(x => x.TypeCode == typeclient);
                                if (typecli.Count() == 0 && !string.IsNullOrEmpty(typeclient))
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else break;
                            }
                            Client clientupd = new Client
                            {
                                Id = cliupd[0].Id,
                                FirstName = (string.IsNullOrEmpty(updfirstName)) ? cliupd[0].FirstName : updfirstName,
                                LastName = (string.IsNullOrEmpty(updlastName)) ? cliupd[0].LastName : updlastName,
                                Patronymic = (string.IsNullOrEmpty(updpatronymic)) ? cliupd[0].Patronymic : updpatronymic,
                                DateOfBirth = (string.IsNullOrEmpty(upddatebirth)) ? cliupd[0].DateOfBirth : _upddatebirth,
                                Address = (string.IsNullOrEmpty(updaddress)) ? cliupd[0].Address : updaddress,
                                Email = (string.IsNullOrEmpty(updemail)) ? cliupd[0].Email : updemail,
                                Passport = (string.IsNullOrEmpty(updpassport)) ? cliupd[0].Passport : updpassport,
                                TypeClientId = (string.IsNullOrEmpty(typeclient)) ? cliupd[0].TypeClientId : typecli[0].Id
                            };
                            cl.Update(clientupd);
                            Console.WriteLine("Клиент изменен");
                            break;
                        case 4:
                            List<Account> accdel;
                            while (true)
                            {
                                Console.WriteLine("Введите ID удаляемого счета.");
                                acc = account.GetList();
                                foreach (Account item in acc)
                                {
                                    var client = cl.GetList(x => x.Id == item.ClientId)[0];
                                    Console.WriteLine($"ID {item.Id}; Номер счета {item.Code}; Клиент - {client.FirstName} {client.LastName};");
                                }
                                int idAcc;
                                try
                                {
                                    idAcc = Convert.ToInt32(Console.ReadLine());
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                accdel = account.GetList(x => x.Id == idAcc);
                                if (accdel.Count() == 0)
                                {
                                    Console.WriteLine("Вы ввели не верно!");
                                    continue;
                                }
                                else break;
                            }
                            Account accdelite = new Account
                            {
                                Id = accdel[0].Id,
                                ClientId = accdel[0].ClientId,
                                CurrencyId = accdel[0].CurrencyId,
                                DateOpen = accdel[0].DateOpen,
                                DateClose = accdel[0].DateClose,
                                StateFl = accdel[0].StateFl,
                                Code = accdel[0].Code,
                                Description = accdel[0].Description
                            };
                            account.Remove(accdelite);

                            break;
                        case 5:
                            while (true)
                            {
                                Console.Write("Все клиенты введите 1, отобрать клиента по ID введите 2:");
                                var c = Console.ReadLine();
                                if (string.IsNullOrEmpty(c) || !int.TryParse(c, out var ch) || ch < 0 || ch > 2)
                                {
                                    Console.WriteLine("Необходимо ввести число 1 или 2.");
                                    continue;
                                }
                                switch (ch)
                                {
                                    case 1:
                                        foreach (Client item in cl.GetList())
                                        {
                                            Console.WriteLine($"ID {item.Id}; Фамилия - {item.FirstName}; Имя - {item.LastName};");
                                        }
                                        break;
                                    case 2:
                                        List<Client> _idclient;
                                        while (true)
                                        {
                                            Console.Write("Введите ID клиента:");
                                            int idclient;
                                            try
                                            {
                                                idclient = Convert.ToInt32(Console.ReadLine());
                                            }
                                            catch (Exception)
                                            {
                                                Console.WriteLine("Вы ввели не верно!");
                                                continue;
                                            }
                                            _idclient = cl.GetList(x => x.Id == idclient);
                                            if (_idclient.Count() == 0)
                                            {
                                                Console.WriteLine("Вы ввели не верно!");
                                                continue;
                                            }
                                            else break;
                                        }
                                        foreach (Client item in _idclient)
                                        {
                                            Console.WriteLine($"ID {item.Id}; Фамилия - {item.FirstName}; Имя - {item.LastName};");
                                        }
                                        break;

                                }
                                break;
                            }
                            break;
                    }
                    if (choice == 6) return; else continue;
                }
            }
        }
    }
}