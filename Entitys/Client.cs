﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6.Entitys
{
    public class Client
    {
        public Client()
        {
            Accounts = new HashSet<Account>();
        }
        public int Id { get; set; }
        public bool ResidFl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; } 
        public string Email { get; set; }
        public string Passport { get; set; }
        public int TypeClientId { get; set; }
        public TypeClient TypeClient { get; set; }
        public ICollection<Account> Accounts { get; set; }

    }
}
