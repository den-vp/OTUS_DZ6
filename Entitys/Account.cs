﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6.Entitys
{
    public class Account
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public int ClientId { get; set; }
        public Currency Currency { get; set; }
        public int CurrencyId { get; set; }
        public DateTime DateOpen { get; set; }
        public DateTime DateClose { get; set; }
        public bool StateFl { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
