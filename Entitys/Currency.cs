﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ6.Entitys
{
    public class Currency
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LongName { get; set; }

    }
}
